﻿using UnityEngine;

namespace Overnight
{
    /// <summary>
    /// Base class for Engine's Behaviour
    /// </summary>
    public abstract class EngineBehaviour : MonoBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        [SerializeField] private bool debugEnabled = false;
        #endregion

        #region PROPERTIES
        #endregion

        protected virtual void Awake()
        {
            Debug.Log("Awakening " + GetType().Name + "...", debugEnabled);
        }

        protected virtual void Start()
        {
            Debug.Log("Starting " + GetType().Name + "...", debugEnabled);
        }

        protected virtual void Update()
        {
            if (debugEnabled)
            {
                OnDebug();
            }
        }

        protected virtual void OnDebug()
        {

        }
    }
}
