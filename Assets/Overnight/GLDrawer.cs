﻿using System.Collections.Generic;
using UnityEngine;

namespace Overnight
{
    public class GLDrawer : MonoBehaviour
    {
        private struct Line
        {
            private readonly Vector3 start;
            private readonly Vector3 end;
            private readonly Color color;
            private readonly float thickness;

            public Line(Vector3 start, Vector3 end, Color color, float thickness = 1f)
            {
                this.start = start;
                this.end = end;
                this.color = color;
                this.thickness = thickness;
            }

            public void Draw()
            {
                GL.Color(color);

                if (thickness == 1.0f)
                {
                    GL.Vertex(start);
                    GL.Vertex(end);
                }
                else
                {
                    float distance = Vector3.Distance(start, end);
                    Vector3 direction = (end - start).normalized;
                    Vector3 normal = Vector3.up;
                    Vector3 cross = Vector3.Cross(direction, normal);

                    Vector3 vector1 = start + cross * thickness;
                    Vector3 vector2 = start - cross * thickness;
                    Vector3 vector3 = end - cross * thickness;
                    Vector3 vector4 = end + cross * thickness;

                    GL.Vertex(vector1);
                    GL.Vertex(vector2);
                    GL.Vertex(vector3);
                    GL.Vertex(vector4);
                }
            }
        }

        #region STATICS
        private static GLDrawer instance = null;

        private Queue<Line> lines;
        private Queue<Line> quads;
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        public bool UseDepth;

        private Material lineMaterial;
        #endregion

        #region PROPERTIES
        #endregion

        void Awake()
        {
            if (instance != null)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                instance = this;
                SetMaterial();
            }

            lines = new Queue<Line>();
            quads = new Queue<Line>();
        }

        void OnPostRender()
        {
            Draw();
        }

        void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                Draw();
            }
        }

        private void SetMaterial()
        {
            Shader shader = Shader.Find("Custom/LineShader");
            lineMaterial = new Material(shader)
            {
                hideFlags = HideFlags.HideAndDontSave,
                shader = {hideFlags = HideFlags.HideAndDontSave}
            };
        }

        private void Draw()
        {
            SetMaterial();
            lineMaterial.SetPass(UseDepth ? 0 : 1);

            GL.Begin(GL.QUADS);
            while (quads.Count > 0)
            {
                Line line = quads.Dequeue();
                line.Draw();
            }
            GL.End();

            GL.Begin(GL.LINES);
            while (lines.Count > 0)
            {
                Line line = lines.Dequeue();
                line.Draw();
            }
            GL.End();
        }

        public void DrawLine(Vector3 start, Vector3 end)
        {
            lines.Enqueue(new Line(start, end, Color.black));
        }

        public void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            lines.Enqueue(new Line(start, end, color));
        }

        public void DrawLine(Vector3 start, Vector3 end, float thickness)
        {
            quads.Enqueue(new Line(start, end, Color.black, thickness));
        }

        public void DrawLine(Vector3 start, Vector3 end, Color color, float thickness)
        {
            quads.Enqueue(new Line(start, end, color, thickness));
        }
    }
}
