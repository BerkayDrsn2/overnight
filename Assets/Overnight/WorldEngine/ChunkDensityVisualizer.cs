﻿using UnityEngine;
using Overnight.WorldEngine;
using Overnight.WorldEngine.Core;

public class ChunkDensityVisualizer : MonoBehaviour
{
	#region STATICS
	#endregion

	#region COMPONENTS
    public WorldEngine worldEngine;
	#endregion

	#region GAMEOBJECTS
    #endregion

	#region VARIABLES
    [SerializeField]
    private int range;
    #endregion

	#region PROPERTIES
	#endregion

	void OnDrawGizmos()
    {
        if (!Application.isPlaying)
            return;

        Map map = worldEngine.GetWorld().GetMap();

        if (range > 4)
            range = 4;

        //for (int y = -range; y < range; y++)
        //{
        //    for (int z = -range; z < range; z++)
        //    {
        //        for (int x = -range; x < range; x++)
        //        {
        //            Vector3 offset = new Vector3(x, y, z);
        //            Chunk nearestChunk = map.GetNearestChunk(transform.position + offset);
        //
        //            if (nearestChunk != null)
        //            {
        //                Vector3 chunkPosition = new Vector3(nearestChunk.Index.X * Chunk.SIZE, 0, nearestChunk.Index.Y * Chunk.SIZE);
        //                Block block = map.GetNearestBlock(transform.position + offset, out BlockIndex nearestIndex);
        //                Gizmos.color = new Color(block.Density, block.Density, block.Density, block.Density);
        //                Gizmos.DrawCube(chunkPosition + (Vector3Int)nearestIndex + Vector3.one / 2f, Vector3.one * 1f);
        //            }
        //        }
        //    }
        //}
    }
}

