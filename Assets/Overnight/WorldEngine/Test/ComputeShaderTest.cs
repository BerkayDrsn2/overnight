﻿using UnityEngine;

namespace Overnight.WorldEngine.Test
{
    public class ComputeShaderTest : MonoBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        private ComputeShader shader;
        private ComputeBuffer buffer;
        private int kernelIndex;

        private int threadNum = 2;
        #endregion

        #region PROPERTIES
        #endregion

        void Awake()
        {
            shader = Resources.Load<ComputeShader>("ComputeShader");
            kernelIndex = shader.FindKernel("Test");
        }

        void Start()
        {
            buffer = new ComputeBuffer(64, 3 * sizeof(uint), ComputeBufferType.Append);
            shader.SetBuffer(kernelIndex, "data", buffer);
            shader.Dispatch(kernelIndex, threadNum, threadNum, threadNum);

            Int3[] a = new Int3[64];
            buffer.GetData(a);

            foreach (Int3 int3 in a)
            {
                var b = GameObject.CreatePrimitive(PrimitiveType.Cube);
                b.transform.position = new Vector3(int3.a, int3.b, int3.c);
            }
        }

        public struct Int3
        {
            public uint a;
            public uint b;
            public uint c;

            public override string ToString()
            {
                return $"({a}, {b}, {c})";
            }
        }

        void Update()
        {

        }
    }
}