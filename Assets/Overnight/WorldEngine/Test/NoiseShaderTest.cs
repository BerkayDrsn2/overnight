﻿using UnityEngine;

namespace Overnight.WorldEngine.Test
{
    public class NoiseShaderTest : MonoBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        public ComputeShader shader;

        private new Renderer renderer;
        private RenderTexture renderTexture;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        public int TextureResolution = 128;
        #endregion

        #region PROPERTIES
        #endregion

        void Awake()
        {
            renderer = GetComponent<Renderer>();
            renderer.enabled = true;

            renderTexture = new RenderTexture(TextureResolution, TextureResolution, 24)
            {
                enableRandomWrite = true,
                filterMode = FilterMode.Point
            };
            renderTexture.Create();

            UpdateTextureFromComputeShader();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                UpdateTextureFromComputeShader();
            }
        }

        private void UpdateTextureFromComputeShader()
        {
            int kernelHandle = shader.FindKernel("CSMain");
            shader.SetInt("RandomOffset", (int)(Time.timeSinceLevelLoad * 100));

            shader.SetTexture(kernelHandle, "Result", renderTexture);
            shader.Dispatch(kernelHandle, TextureResolution / 8, TextureResolution / 8, 1);

            // ReSharper disable once Unity.PreferAddressByIdToGraphicsParams
            renderer.material.SetTexture("_MainTex", renderTexture);
        }
    }
}

