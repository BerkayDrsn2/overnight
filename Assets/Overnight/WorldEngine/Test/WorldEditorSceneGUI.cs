﻿using Overnight.WorldEngine.Core;
using UnityEditor;
using UnityEngine;

namespace Overnight.WorldEngine.Test
{
    [RequireComponent(typeof(WorldEngine))]
    [RequireComponent(typeof(WorldEditor))]
    public class WorldEditorSceneGUI : MonoBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private WorldEditor worldEditor;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        public bool Hide;

        private World world;
        private Map map;
        #endregion

        #region PROPERTIES
        #endregion

        void Start()
        {
            worldEditor = GetComponent<WorldEditor>();
            world = GetComponent<WorldEngine>().GetWorld();
            map = world.GetMap();
        }

        void OnEnable()
        {
            SceneView.duringSceneGui += OnSceneGUI;
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        private void OnSceneGUI(SceneView sceneView)
        {
            if (Hide)
            {
                return;
            }

            foreach (Chunk chunk in map)
            {
                ChunkState chunkState = world.GetChunkManager().GetChunkState(chunk.Index);
                if (chunkState.HasFlag(ChunkState.INSTANTIATED) || chunkState.HasFlag(ChunkState.LOADED))
                {
                    foreach ((Block block, BlockIndex index) in map.GetIterator(chunk.Index))
                    {
                        Vector3 blockPosition = new Vector3(chunk.Index.X * map.ChunkSize.x + index.X, index.Y, chunk.Index.Y * map.ChunkSize.z + index.Z) + Vector3.one / 2;
                        Handles.color = Color.green;
                        Handles.DrawWireCube(blockPosition, Vector3.one);
                        Handles.color = chunk[index].Density < 0.5f ? Color.white : Color.black;
                        if (Handles.Button(blockPosition, Quaternion.identity, .125f, .125f, Handles.SphereHandleCap))
                        {
                            worldEditor.EditBlockDensity(index, 1 - chunk[index].Density);
                        }
                    }
                }
            }
        }
    }
}
