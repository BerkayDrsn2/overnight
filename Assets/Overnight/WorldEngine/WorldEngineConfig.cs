﻿using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    [CreateAssetMenu(menuName = "World Engine/Create Config", fileName = "New World Engine Config")]
    public class WorldEngineConfig : ScriptableObject
    {
        public bool GenerateChunkMeshes;

        public int RenderDistance;
        public bool AsyncUpdate;

        public int LoadChunkBatchSize;
        public int InstantiateChunkBatchSize;
        public int UpdateChunkBatchSize;

        public float LoadChunkInterval;
        public float InstantiateChunkInterval;
        public float UpdateChunkInterval;

        public Vector3 Scale;
        public int Octave;
        public float Threshold;
    }
}

