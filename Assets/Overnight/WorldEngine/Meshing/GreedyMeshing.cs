﻿using System;
using System.Collections.Generic;
using Overnight.WorldEngine.Core;
using UnityEngine;

namespace Overnight.WorldEngine.Meshing
{
    public class GreedyMeshing : IMeshing
    {
        private enum Direction
        {
            NORTH = 0,
            EAST  = 1,
            SOUTH = 2,
            WEST  = 3,
            UP    = 4,
            DOWN  = 5
        }

        #region STATICS
        private static readonly Vector3[] vertexData = new Vector3[8]
        {
            new Vector3(0, 0, 0), new Vector3(0, 1, 0),
            new Vector3(1, 1, 0), new Vector3(1, 0, 0),
            new Vector3(0, 0, 1), new Vector3(0, 1, 1),
            new Vector3(1, 1, 1), new Vector3(1, 0, 1)
        };

        private static readonly byte[,] vertexIndexData = new byte[6, 4]
        {
            {7, 6, 5, 4},    // NORTH
            {3, 2, 6, 7},    // EAST
            {0, 1, 2, 3},    // SOUTH
            {4, 5, 1, 0},    // WEST
            {1, 5, 6, 2},    // UP
            {4, 0, 3, 7}     // DOWN
        };

        private static readonly Vector3[] normalData = new Vector3[6]
        {
            Vector3.forward, // NORTH
            Vector3.right,   // EAST
            Vector3.back,    // SOUTH
            Vector3.left,    // WEST
            Vector3.up,      // UP
            Vector3.down     // DOWN
        };
        #endregion

        #region VARIABLES
        private readonly Map map;
        #endregion

        public GreedyMeshing(Map map)
        {
            this.map = map;
        }

        public Mesh GenerateMesh(ChunkIndex chunkIndex)
        {
            byte[,,] visibilityFlags = GenerateVisibilityFlags(map[chunkIndex]);
            Mesh mesh = GenerateMesh(visibilityFlags);
            return mesh;
        }

        private byte[,,] GenerateVisibilityFlags(Chunk chunk)
        {
            byte[,,] visibilityFlag = new byte[map.ChunkSize.x, map.ChunkSize.y, map.ChunkSize.z];

            foreach ((Block block, BlockIndex blockIndex) in map.GetIterator(chunk.Index))
            {
                if (block.Density >= 0.5f)
                {
                    Block[] adjBlocks = map.GetNeighborBlocks(blockIndex);
                    for (int i = 0; i < 6; i++)
                    {
                        if (adjBlocks[i].Density <= 0.5f)
                        {
                            visibilityFlag[blockIndex.X, blockIndex.Y, blockIndex.Z] |= (byte)(1 << i);
                        }
                    }
                }
            }

            return visibilityFlag;
        }

        private Mesh GenerateMesh(byte[,,] visibilityFlag)
        {
            List<Mesh> voxelFaces = new List<Mesh>();
            Array directions = Enum.GetValues(typeof(Direction));

            foreach (Direction direction in directions)
            {
                int directionMask = 1 << (int) direction;

                if (direction == Direction.DOWN || direction == Direction.UP)
                {
                    for (int y = 0; y < map.ChunkSize.y; y++)
                    {
                        for (int z = 0; z < map.ChunkSize.z; z++)
                        {
                            for (int x = 0; x < map.ChunkSize.x; x++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeX = 0;
                                    int sizeZ = 1;

                                    int mz = z;
                                    int mx = x;

                                    for (mx = x;
                                        mx < map.ChunkSize.x && (visibilityFlag[mx, y, mz] & directionMask) != 0;
                                        mx++)
                                    {
                                        sizeX++;
                                    }

                                    for (mz = z + 1, mx = x; mz < map.ChunkSize.z && mx < x + sizeX; mx++)
                                    {
                                        if ((visibilityFlag[mx, y, mz] & directionMask) == 0)
                                            break;

                                        if (mx == x + sizeX - 1)
                                        {
                                            sizeZ++;
                                            mz++;
                                            mx = x - 1;
                                        }
                                    }

                                    for (mz = z; mz < z + sizeZ; mz++)
                                    {
                                        for (mx = x; mx < x + sizeX; mx++)
                                        {
                                            visibilityFlag[mx, y, mz] &= (byte) ~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeX, sizeZ));
                                    voxelFaces.Add(voxelFace);

                                    x = mx - 1;
                                }
                            }
                        }
                    }
                }
                else if (direction == Direction.EAST || direction == Direction.WEST)
                {
                    for (int x = 0; x < map.ChunkSize.x; x++)
                    {
                        for (int y = 0; y < map.ChunkSize.y; y++)
                        {
                            for (int z = 0; z < map.ChunkSize.z; z++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeZ = 0;
                                    int sizeY = 1;

                                    int mz = z;
                                    int my = y;

                                    for (mz = z;
                                        mz < map.ChunkSize.z && (visibilityFlag[x, my, mz] & directionMask) != 0;
                                        mz++)
                                    {
                                        sizeZ++;
                                    }

                                    for (my = y + 1, mz = z; my < map.ChunkSize.y && mz < z + sizeZ; mz++)
                                    {
                                        if ((visibilityFlag[x, my, mz] & directionMask) == 0)
                                            break;

                                        if (mz == z + sizeZ - 1)
                                        {
                                            sizeY++;
                                            my++;
                                            mz = z - 1;
                                        }
                                    }

                                    for (my = y; my < y + sizeY; my++)
                                    {
                                        for (mz = z; mz < z + sizeZ; mz++)
                                        {
                                            visibilityFlag[x, my, mz] &= (byte) ~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeZ, sizeY));
                                    voxelFaces.Add(voxelFace);

                                    z = mz - 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int z = 0; z < map.ChunkSize.z; z++)
                    {
                        for (int y = 0; y < map.ChunkSize.y; y++)
                        {
                            for (int x = 0; x < map.ChunkSize.x; x++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeX = 0;
                                    int sizeY = 1;

                                    int mx = x;
                                    int my = y;

                                    for (mx = x;
                                        mx < map.ChunkSize.x && (visibilityFlag[mx, my, z] & directionMask) != 0;
                                        mx++)
                                        sizeX++;

                                    for (my = y + 1, mx = x; my < map.ChunkSize.y && mx < x + sizeX; mx++)
                                    {
                                        if ((visibilityFlag[mx, my, z] & directionMask) == 0)
                                            break;

                                        if (mx == x + sizeX - 1)
                                        {
                                            sizeY++;
                                            my++;
                                            mx = x - 1;
                                        }
                                    }

                                    for (my = y; my < y + sizeY; my++)
                                    {
                                        for (mx = x; mx < x + sizeX; mx++)
                                        {
                                            visibilityFlag[mx, my, z] &= (byte) ~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeX, sizeY));
                                    voxelFaces.Add(voxelFace);

                                    x = mx - 1;
                                }
                            }
                        }
                    }
                }
            }

            List<CombineInstance> combineInstances = new List<CombineInstance>();
            foreach (Mesh voxelFace in voxelFaces)
            {
                CombineInstance instance = new CombineInstance()
                {
                    mesh = voxelFace,
                    transform = GameObject.Find("World Engine").transform.localToWorldMatrix
                };

                combineInstances.Add(instance);
            }

            Mesh mesh = new Mesh();
            mesh.CombineMeshes(combineInstances.ToArray());
            return mesh;
        }

        private Mesh GetVoxelFace(Direction direction, Vector3 position, Vector2 size)
        {
            Mesh faceMesh = new Mesh();

            Vector3[] vertices = new Vector3[4];
            for (int i = 0; i < 4; i++)
            {
                Vector3 vertex = vertexData[vertexIndexData[(int) direction, i]];
                if (direction == Direction.UP || direction == Direction.DOWN)
                {
                    vertex.x *= size.x;
                    vertex.z *= size.y;
                }
                else if (direction == Direction.NORTH || direction == Direction.SOUTH)
                {
                    vertex.x *= size.x;
                    vertex.y *= size.y;
                }
                else
                {
                    vertex.z *= size.x;
                    vertex.y *= size.y;
                }

                vertices[i] = position + vertex;
            }

            int[] triangles = new int[6] {0, 1, 2, 0, 2, 3};

            Vector3 normal = normalData[(int) direction];
            Vector3[] normals =
            {
                normal,
                normal,
                normal,
                normal
            };

            Vector2[] uv =
            {
                new Vector2(0, 0),
                new Vector2(size.y, 0),
                new Vector2(size.y, size.x),
                new Vector2(0, size.x)
            };

            faceMesh.vertices = vertices;
            faceMesh.triangles = triangles;
            faceMesh.normals = normals;
            faceMesh.uv = uv;

            return faceMesh;
        }
    }
}