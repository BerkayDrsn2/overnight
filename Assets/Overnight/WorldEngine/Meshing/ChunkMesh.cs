﻿using Overnight.WorldEngine.Core;
using UnityEngine;

namespace Overnight.WorldEngine.Meshing
{
    public struct ChunkMesh
    {
        #region STATICS
        #endregion

        #region VARIABLES
        public Chunk Chunk;
        public Mesh Mesh;
        #endregion

        #region PROPERTIES
        #endregion

        public ChunkMesh(Chunk chunk, Mesh mesh)
        {
            Chunk = chunk;
            Mesh = mesh;
        }
    }
}