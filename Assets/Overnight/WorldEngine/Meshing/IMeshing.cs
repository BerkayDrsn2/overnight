﻿using Overnight.WorldEngine.Core;
using UnityEngine;

namespace Overnight.WorldEngine.Meshing
{
    public interface IMeshing
    {
        Mesh GenerateMesh(ChunkIndex chunkIndex);
    }
}
