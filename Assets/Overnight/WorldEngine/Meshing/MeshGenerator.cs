﻿using System.Collections.Generic;
using Overnight.WorldEngine.Core;
using UnityEngine;

namespace Overnight.WorldEngine.Meshing
{
    public class MeshGenerator
    {
        public enum MeshingMode
        {
            GREEDY_MESHING = 0,
            MARCHING_CUBES_CPU = 1,
            MARCHING_CUBES_GPU = 2,
            DUAL_CONTOURING_CPU = 3,
            DUAL_CONTOURING_GPU = 4
        }

        #region STATICS
        #endregion

        #region VARIABLES
        private readonly Map map;
        private readonly Dictionary<MeshingMode, IMeshing> meshingModes;

        private IMeshing currentMeshing;
        #endregion

        #region PROPERTIES
        #endregion

        public MeshGenerator(Map map)
        {
            this.map = map;
            meshingModes = new Dictionary<MeshingMode, IMeshing>
            {
                { MeshingMode.GREEDY_MESHING, new GreedyMeshing(map) },
                { MeshingMode.MARCHING_CUBES_CPU, new MarchingCubes(map, false) },
                { MeshingMode.MARCHING_CUBES_GPU, new MarchingCubes(map, true) },
            };

            SetMeshingMode(MeshingMode.MARCHING_CUBES_GPU);
        }

        public void SetMeshingMode(MeshingMode meshingMode)
        {
            Debug.Assert(meshingModes.ContainsKey(meshingMode), "Meshing Mode not yet implemented.");
            currentMeshing = meshingModes[meshingMode];
        }

        public ChunkMesh GenerateChunkMesh(ChunkIndex chunkIndex)
        {
            Mesh mesh = currentMeshing.GenerateMesh(chunkIndex);
            return new ChunkMesh(map[chunkIndex], mesh);
        }
    }
}
