﻿using Overnight.WorldEngine.Core;
using UnityEngine;

namespace Overnight.WorldEngine
{
    public class WorldEngine : EngineBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        public WorldEngineConfig Config;
        public Vector3Int ChunkSize;

        private World world;
        #endregion

        #region PROPERTIES
        #endregion

        protected override void Awake()
        {
            base.Awake();
            world = new World(this);

            Debug.GLEnabled = true;
        }

        protected override void Start()
        {
            base.Start();
            world.Initialize();
        }

        protected override void Update()
        {
            world.Update();
            base.Update();
        }

        protected override void OnDebug()
        {
            world.OnDebug();
        }

        public World GetWorld()
        {
            Debug.Assert(world != null);
            return world;
        }
    }
}
