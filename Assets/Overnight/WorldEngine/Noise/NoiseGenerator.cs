﻿using System.Runtime.InteropServices;
using Overnight.WorldEngine.Core;
using UnityEngine;

public class NoiseGenerator
{
    #region STATICS
    private static int kernelGroupSize = 4;
    #endregion

    #region COMPONENTS
    private World world;
    private Map map;
	#endregion

	#region GAMEOBJECTS
	#endregion

	#region VARIABLES
    private Vector3Int chunkSize;
    private bool useGPU;

    private ComputeShader shader;
    private ComputeBuffer buffer;
    private int kernelIndex;
    #endregion

    #region PROPERTIES
    #endregion

    public NoiseGenerator(World world, bool useGPU)
    {
        this.world = world;
        this.useGPU = useGPU;

        map = world.GetMap();
        chunkSize = map.ChunkSize;
        shader = Resources.Load<ComputeShader>("RandomNoise");
        kernelIndex = shader.FindKernel("CSMain");
    }

    public void GenerateNoise(Chunk chunk, int seed, int octave)
    {
        buffer = new ComputeBuffer(map.ChunkSize.x * map.ChunkSize.y * map.ChunkSize.z, Marshal.SizeOf(typeof(Block)));
        shader.SetInts("chunkIndex", chunk.Index.X, chunk.Index.Y);
        shader.SetInts("chunkSize", chunkSize.x, chunkSize.z);
        shader.SetInt("seed", seed);
        shader.SetInt("octave", octave);
        shader.SetBuffer(kernelIndex, "noise", buffer);
        shader.Dispatch(kernelIndex, kernelGroupSize, kernelGroupSize, kernelGroupSize);
        buffer.GetData(chunk);
        buffer.Release();
    }
}

