﻿using UnityEditor;
using UnityEngine;

namespace Overnight.WorldEngine
{
    [CustomEditor(typeof(WorldEngine))]
    public class WorldEngineInspector : Editor
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private WorldEngine worldEngine;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        #endregion

        #region PROPERTIES
        #endregion

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            worldEngine = (WorldEngine)target;

            worldEngine.Config.GenerateChunkMeshes = EditorGUILayout.Toggle("Generate Chunk Meshes", worldEngine.Config.GenerateChunkMeshes);


            int newRenderDistance = EditorGUILayout.IntField("Render Distance", worldEngine.Config.RenderDistance);
            if (newRenderDistance != worldEngine.Config.RenderDistance)
            {
                worldEngine.Config.RenderDistance = newRenderDistance;
                worldEngine.GetWorld()?.ForceUpdate();
            }

            worldEngine.Config.AsyncUpdate = EditorGUILayout.Toggle("Async Update", worldEngine.Config.AsyncUpdate);

            worldEngine.Config.Scale = EditorGUILayout.Vector3Field("Scale", worldEngine.Config.Scale);
            worldEngine.Config.Octave = EditorGUILayout.IntField("Octave", worldEngine.Config.Octave);
            worldEngine.Config.Threshold = EditorGUILayout.FloatField("Threshold", worldEngine.Config.Threshold);

            worldEngine.Config.LoadChunkBatchSize = EditorGUILayout.IntField("Load Chunk Batch Size", worldEngine.Config.LoadChunkBatchSize);
            worldEngine.Config.InstantiateChunkBatchSize = EditorGUILayout.IntField("Instantiate Chunk BatchSize", worldEngine.Config.InstantiateChunkBatchSize);
            worldEngine.Config.UpdateChunkBatchSize = EditorGUILayout.IntField("Update Chunk BatchSize", worldEngine.Config.UpdateChunkBatchSize);

            worldEngine.Config.LoadChunkInterval = EditorGUILayout.FloatField("Load Chunk Interval", worldEngine.Config.LoadChunkInterval);
            worldEngine.Config.InstantiateChunkInterval = EditorGUILayout.FloatField("Instantiate Chunk Interval", worldEngine.Config.InstantiateChunkInterval);
            worldEngine.Config.UpdateChunkInterval = EditorGUILayout.FloatField("Update Chunk Interval", worldEngine.Config.UpdateChunkInterval);

            if (GUILayout.Button("Reset"))
            {
                worldEngine.GetWorld()?.Reset();
            }
        }
    }
}

