﻿using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    /// <summary>
    ///   <para>A Chunk defines the region of Blocks in a fixed size volume inside the space.</para>
    /// </summary>
    public class Chunk
    {
        #region STATICS
        #endregion

        #region VARIABLES
        /// <summary>
        ///   Internal Block data array of this Chunk.
        /// </summary>
        private readonly Block[,,] blockData;
        #endregion

        #region PROPERTIES
        /// <summary>
        ///   Index of this Chunk in Map.
        /// </summary>
        public ChunkIndex Index { get; }
        #endregion

        #region INDEXERS
        public Block this[BlockIndex blockIndex]
        {
            get { return blockData[blockIndex.X, blockIndex.Y, blockIndex.Z]; }
        }

        public Block this[int x, int y, int z]
        {
            get { return blockData[x, y, z]; }
        }
        #endregion

        public Chunk(ChunkIndex index, Vector3Int size)
        {
            Index = index;
            blockData = new Block[size.x, size.y, size.z];
            Initialize(size);
        }

        /// <summary>
        ///   Initializes Chunk space with empty Blocks.
        /// </summary>
        /// <param name="size"></param>
        private void Initialize(Vector3Int size)
        {
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    for (int x = 0; x < size.x; x++)
                    {
                        blockData[x, y, z] = Block.Empty;
                    }
                }
            }
        }

        /// <summary>
        ///   <para>Set ID of a Block at given index.</para>
        /// </summary>
        /// <param name="index"></param>
        /// <param name="id"></param>
        public void SetBlockID(BlockIndex index, byte id)
        {
            blockData[index.X, index.Y, index.Z].ID = id;
        }

        /// <summary>
        ///   <para>Set density of a Block at given index.</para>
        /// </summary>
        /// <param name="index"></param>
        /// <param name="density"></param>
        public void SetBlockDensity(BlockIndex index, float density)
        {
            blockData[index.X, index.Y, index.Z].Density = density;
        }

        /// <summary>
        ///   <para>Returns a nicely formatted string for this Chunk.</para>
        /// </summary>
        public override string ToString()
        {
            return $"{Index}";
        }

        /// <summary>
        ///   <para>Implicit operator for casting this Chunk by exposing it's data array.</para>
        /// </summary>
        /// <param name="chunk"></param>
        public static implicit operator Block[,,](Chunk chunk)
        {
            return chunk.blockData;
        }
    }
}
