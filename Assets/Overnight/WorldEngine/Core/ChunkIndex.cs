﻿using System;
using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    /// <summary>
    ///   <para>Representation of Chunk Index.</para>
    /// </summary>
    public struct ChunkIndex : IEquatable<ChunkIndex>
    {
        #region VARIABLES
        private int x;
        private int y;
        #endregion

        #region PROPERTIES
        /// <summary>
        ///   <para>X component of the index.</para>
        /// </summary>
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        /// <summary>
        ///   <para>Y component of the index.</para>
        /// </summary>
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(0, 0).</para>
        /// </summary>
        public static ChunkIndex Zero { get; } = new ChunkIndex(0, 0);

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(1, 1).</para>
        /// </summary>
        public static ChunkIndex One { get; } = new ChunkIndex(1, 1);

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(0, 1).</para>
        /// </summary>
        public static ChunkIndex Up { get; } = new ChunkIndex(0, 1);

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(0, -1).</para>
        /// </summary>
        public static ChunkIndex Down { get; } = new ChunkIndex(0, -1);

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(-1, 0).</para>
        /// </summary>
        public static ChunkIndex Left { get; } = new ChunkIndex(-1, 0);

        /// <summary>
        ///   <para>Shorthand for writing ChunkIndex(1, 0).</para>
        /// </summary>
        public static ChunkIndex Right { get; } = new ChunkIndex(1, 0);

        /// <summary>
        ///   <para>Shorthand for writing this + ChunkIndex(0, 1).</para>
        /// </summary>
        public ChunkIndex up { get { return this + Up; } }

        /// <summary>
        ///   <para>Shorthand for writing this + ChunkIndex(0, -1).</para>
        /// </summary>
        public ChunkIndex down { get { return this + Down; } }

        /// <summary>
        ///   <para>Shorthand for writing this + ChunkIndex(-1, 0).</para>
        /// </summary>
        public ChunkIndex left { get { return this + Left; } }

        /// <summary>
        ///   <para>Shorthand for writing this + ChunkIndex(1, 0).</para>
        /// </summary>
        public ChunkIndex right { get { return this + Right; } }
        #endregion

        #region OPERATORS
        public static implicit operator Vector2Int(ChunkIndex index)
        {
            return new Vector2Int(index.x, index.y);
        }

        public static implicit operator Vector2(ChunkIndex index)
        {
            return new Vector2(index.x, index.y);
        }

        public static explicit operator Vector3Int(ChunkIndex index)
        {
            return new Vector3Int(index.x, 0, index.y);
        }

        public static explicit operator Vector3(ChunkIndex index)
        {
            return new Vector3(index.x, 0, index.y);
        }

        public static ChunkIndex operator +(ChunkIndex lhs, ChunkIndex rhs)
        {
            return new ChunkIndex(lhs.x + rhs.x, lhs.y + rhs.y);
        }

        public static ChunkIndex operator -(ChunkIndex lhs, ChunkIndex rhs)
        {
            return new ChunkIndex(lhs.x - rhs.x, lhs.y - rhs.y);
        }

        public static ChunkIndex operator *(ChunkIndex lhs, int value)
        {
            return new ChunkIndex(lhs.x * value, lhs.y * value);
        }

        public static bool operator ==(ChunkIndex lhs, ChunkIndex rhs)
        {
            return lhs.x == rhs.x && lhs.y == rhs.y;
        }

        public static bool operator !=(ChunkIndex lhs, ChunkIndex rhs)
        {
            return !(lhs == rhs);
        }
        #endregion

        public ChunkIndex(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        ///   <para>Set x and y components of an existing Chunk Index.</para>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Set(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        ///   <para>Returns a nicely formatted string for this Chunk Index.</para>
        /// </summary>
        public override string ToString()
        {
            return $"({x}, {y})";
        }

        public bool Equals(ChunkIndex other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            return obj is ChunkIndex other && Equals(other);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() << 2;
        }
    }
}
