﻿using System;

namespace Overnight.WorldEngine.Core
{
    [Flags]
    public enum ChunkState : byte
    {
        INITIALIZED           = 0,
        READY_FOR_LOAD        = 1,
        LOADED                = 2,
        READY_FOR_UNLOAD      = 4,
        READY_FOR_INSTANTIATE = 8,
        INSTANTIATED          = 16,
        READY_FOR_REMOVE      = 32
    }
}
