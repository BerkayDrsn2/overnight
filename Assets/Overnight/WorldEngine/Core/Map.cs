﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class Map : IEnumerable<Chunk>
    {
        #region STATICS
        private IList<Vector3Int> neighborIndexList = new List<Vector3Int>()
        {
            new Vector3Int(0, 0, 1),
            new Vector3Int(1, 0, 0),
            new Vector3Int(0, 0, -1),
            new Vector3Int(-1, 0, 0),
            new Vector3Int(0, 1, 0),
            new Vector3Int(0, -1, 0)
        };
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        /// <summary>
        ///   Internal Chunk data map of this Map.
        /// </summary>
        private readonly Dictionary<ChunkIndex, Chunk> chunkMap;
        #endregion

        #region PROPERTIES
        /// <summary>
        ///   Size of the Chunks that this Map uses.
        /// </summary>
        public Vector3Int ChunkSize { get; }
        #endregion

        #region INDEXERS
        public Chunk this[ChunkIndex index]
        {
            get
            {
                chunkMap.TryGetValue(index, out Chunk chunk);
                return chunk;
            }
        }

        public Block this[ChunkIndex index, BlockIndex blockIndex]
        {
            get { return this[index][blockIndex]; }
        }
        #endregion

        public Map(Vector3Int chunkSize)
        {
            chunkMap = new Dictionary<ChunkIndex, Chunk>();
            ChunkSize = chunkSize;
        }

        public bool ContainsChunk(ChunkIndex index)
        {
            return chunkMap.ContainsKey(index);
        }

        public Chunk CreateChunk(ChunkIndex index)
        {
            Debug.Assert(!ContainsChunk(index), "Chunk already exists");
            Chunk chunk = new Chunk(index, ChunkSize);
            chunkMap.Add(index, chunk);
            return chunk;
        }

        public void Clear()
        {
            chunkMap.Clear();
        }

        public ChunkIndex GetChunkIndexAt(Vector3 position)
        {
            return new ChunkIndex(Mathf.FloorToInt(position.x / ChunkSize.x),
                                  Mathf.FloorToInt(position.z / ChunkSize.z));
        }

        public BlockIndex GetBlockIndexAt(Vector3 position)
        {
            int x = Mathf.FloorToInt(position.x % ChunkSize.x + (position.x < 0 ? ChunkSize.x : 0));
            int y = Mathf.FloorToInt(Mathf.Clamp(position.y, 0, ChunkSize.y - 1));
            int z = Mathf.FloorToInt(position.z % ChunkSize.z + (position.z < 0 ? ChunkSize.z : 0));


            return new BlockIndex(x, y, z, GetChunkIndexAt(position));
        }

        public Vector3 GetBlockPositionAt(BlockIndex blockIndex)
        {
            ChunkIndex chunkIndex = blockIndex.GetChunkIndex();
            return new Vector3(chunkIndex.X * ChunkSize.x + blockIndex.X, blockIndex.Y, chunkIndex.Y * ChunkSize.z + blockIndex.Z);
        }

        public Chunk GetNearestChunk(Vector3 position)
        {
            ChunkIndex nearestIndex = GetChunkIndexAt(position);
            if (chunkMap.TryGetValue(nearestIndex, out Chunk closestChunk))
            {
                return closestChunk;
            }

            float minDistance = float.MaxValue;
            foreach (Chunk chunk in chunkMap.Values)
            {
                float distance = Vector2.Distance(nearestIndex, chunk.Index);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestChunk = chunk;
                }
            }

            return closestChunk;
        }

        public (Block, BlockIndex) GetNearestBlock(Vector3 position)
        {
            BlockIndex nearestIndex = GetBlockIndexAt(position);
            return (chunkMap[nearestIndex.GetChunkIndex()][nearestIndex], nearestIndex);
        }

        public Block[] GetNeighborBlocks(BlockIndex blockIndex)
        {
            return GetIterator(blockIndex, neighborIndexList).Select(x => x.Item1).ToArray();
        }

        public BlockIndex AdjustIndex(BlockIndex blockIndex)
        {
            ChunkIndex chunkIndex = blockIndex.GetChunkIndex();

            while (blockIndex.X < 0)
            {
                chunkIndex = chunkIndex.left;
                blockIndex.X += ChunkSize.x;
            }

            while (blockIndex.X > ChunkSize.x - 1)
            {
                chunkIndex = chunkIndex.right;
                blockIndex.X -= ChunkSize.x;
            }

            while (blockIndex.Z < 0)
            {
                chunkIndex = chunkIndex.down;
                blockIndex.Z += ChunkSize.z;
            }

            while (blockIndex.Z > ChunkSize.z - 1)
            {
                chunkIndex = chunkIndex.up;
                blockIndex.Z -= ChunkSize.z;
            }

            blockIndex.SetChunkIndex(chunkIndex);
            return blockIndex;
        }

        public BlockIterator GetIterator(ChunkIndex chunkIndex)
        {
            return new BlockIterator(this, chunkIndex);
        }

        public BlockIterator GetIterator(BlockIndex blockIndex, Vector3Int size, bool biDirectional = true)
        {
            return new BlockIterator(this, blockIndex, size, biDirectional);
        }

        public BlockIterator GetIterator(BlockIndex blockIndex, IList<Vector3Int> indexList)
        {
            return new BlockIterator(this, blockIndex, indexList);
        }
        
        public IEnumerator<Chunk> GetEnumerator()
        {
            return chunkMap.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
