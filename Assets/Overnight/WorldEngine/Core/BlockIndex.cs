﻿using System;
using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    /// <summary>
    ///   <para>Representation of Block Index.</para>
    /// </summary>
    public struct BlockIndex : IEquatable<BlockIndex>
    {
        #region VARIABLES
        private int x;
        private int y;
        private int z;
        private ChunkIndex chunkIndex;
        #endregion

        #region PROPERTIES
        /// <summary>
        ///   <para>X component of the Block Index.</para>
        /// </summary>
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        /// <summary>
        ///   <para>Y component of the Block Index.</para>
        /// </summary>
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        ///   <para>Z component of the Block Index.</para>
        /// </summary>
        public int Z
        {
            get { return z; }
            set { z = value; }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(0, 1, 0).</para>
        /// </summary>
        public BlockIndex Up
        {
            get { return new BlockIndex(x, y + 1, z, chunkIndex); }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(0, -1, 0).</para>
        /// </summary>
        public BlockIndex Down
        {
            get { return new BlockIndex(x, y - 1, z, chunkIndex); }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(-1, 0, 0).</para>
        /// </summary>
        public BlockIndex Left
        {
            get { return new BlockIndex(x - 1, y, z, chunkIndex); }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(1, 0, 0).</para>
        /// </summary>
        public BlockIndex Right
        {
            get { return new BlockIndex(x + 1, y, z, chunkIndex); }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(0, 0, 1).</para>
        /// </summary>
        public BlockIndex Forward
        {
            get { return new BlockIndex(x, y, z + 1, chunkIndex); }
        }

        /// <summary>
        ///   <para>Shorthand for writing this + BlockIndex(0, 0, -1).</para>
        /// </summary>
        public BlockIndex Backward
        {
            get { return new BlockIndex(x, y, z - 1, chunkIndex); }
        }
        #endregion

        #region OPERATORS
        public static explicit operator Vector2Int(BlockIndex index)
        {
            return new Vector2Int(index.x, index.z);
        }
        
        public static implicit operator Vector3Int(BlockIndex index)
        {
            return new Vector3Int(index.x, index.y, index.z);
        }
        
        public static implicit operator BlockIndex(Vector3Int vector)
        {
            return new BlockIndex(vector.x, vector.y, vector.z);
        }
        
        public static explicit operator Vector2(BlockIndex index)
        {
            return new Vector2(index.x, index.z);
        }
        
        public static implicit operator Vector3(BlockIndex index)
        {
            return new Vector3(index.x, index.y, index.z);
        }

        public static BlockIndex operator +(BlockIndex index, Vector3Int vector)
        {
            return new BlockIndex(index.x + vector.x, index.y + vector.y, index.z + vector.z, index.chunkIndex);
        }

        public static BlockIndex operator -(BlockIndex index, Vector3Int vector)
        {
            return new BlockIndex(index.x - vector.x, index.y - vector.y, index.z - vector.z, index.chunkIndex);
        }

        public static BlockIndex operator *(BlockIndex index, int value)
        {
            return new BlockIndex(index.x * value, index.y * value, index.z * value, index.chunkIndex);
        }

        public static bool operator ==(BlockIndex lhs, BlockIndex rhs)
        {
            return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.chunkIndex == rhs.chunkIndex;
        }

        public static bool operator !=(BlockIndex lhs, BlockIndex rhs)
        {
            return !(lhs == rhs);
        }
        #endregion

        public BlockIndex(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            chunkIndex = ChunkIndex.Zero;
        }

        public BlockIndex(int x, int y, int z, ChunkIndex chunkIndex)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.chunkIndex = chunkIndex;
        }

        /// <summary>
        ///   <para>Set x, y and z components of an existing Block Index.</para>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Set(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        ///   <para>Set Chunk Index of an existing Block Index.</para>
        /// </summary>
        /// <param name="chunkIndex"></param>
        public void SetChunkIndex(ChunkIndex chunkIndex)
        {
            this.chunkIndex = chunkIndex;
        }

        /// <summary>
        ///   <para>Get Chunk Index of an existing Block Index.</para>
        /// </summary>
        /// <param name="chunkIndex"></param>
        public ChunkIndex GetChunkIndex()
        {
             return chunkIndex;
        }

        /// <summary>
        ///   <para>Returns a nicely formatted string for this Block Index.</para>
        /// </summary>
        public override string ToString()
        {
            return $"({x}, {y}, {z}) at Chunk: {chunkIndex.ToString()})";
        }

        public bool Equals(BlockIndex other)
        {
            return x == other.x && y == other.y && z == other.z && chunkIndex == other.chunkIndex;
        }

        public override bool Equals(object obj)
        {
            return obj is BlockIndex other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = x;
                hashCode = (hashCode * 397) ^ y;
                hashCode = (hashCode * 397) ^ z;
                hashCode = (hashCode * 397) ^ chunkIndex.GetHashCode();
                return hashCode;
            }
        }
    }
}
