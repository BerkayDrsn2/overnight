﻿using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class ChunkObject
    {
        public ChunkState State;
        public Chunk Chunk;
        public GameObject Object;
        public bool Dirty;
    }
}

