﻿using Overnight.WorldEngine.Meshing;
using System.Collections.Generic;
using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class ChunkManager
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private readonly World world;
        private readonly Map map;
        private readonly WorldGenerator worldGenerator;
        private readonly MeshGenerator meshGenerator;
        #endregion

        #region GAMEOBJECTS
        private GameObject worldObject;
        #endregion

        #region VARIABLES
        private ChunkIndex originIndex;
        private int renderDistance;

        private readonly Dictionary<ChunkIndex, ChunkObject> chunkObjectMap;
        private readonly List<ChunkIndex> visibleChunkList;
        private readonly Queue<ChunkIndex> loadChunkQueue;
        private readonly Queue<ChunkIndex> instantiateChunkQueue;
        private readonly Queue<ChunkIndex> updateChunkQueue;

        private float loadChunkCycleClock;
        private float instantiateChunkCycleClock;
        private float updateChunkCycleClock;

        private bool updateVisibleChunkList;
        #endregion

        #region PROPERTIES
        public ChunkIndex OriginIndex
        {
            get { return originIndex; }
            set
            {
                if (originIndex != value)
                {
                    originIndex = value;
                    updateVisibleChunkList = true;
                }
            }
        }
        #endregion

        public ChunkManager(World world)
        {
            this.world = world;

            map = world.GetMap();
            worldGenerator = world.GetWorldGenerator();
            worldObject = world.GetWorldObject();

            meshGenerator = new MeshGenerator(map);
            chunkObjectMap = new Dictionary<ChunkIndex, ChunkObject>();

            visibleChunkList = new List<ChunkIndex>();
            loadChunkQueue = new Queue<ChunkIndex>();
            instantiateChunkQueue = new Queue<ChunkIndex>();
            updateChunkQueue = new Queue<ChunkIndex>();

            updateVisibleChunkList = true;
        }

        public void Update(float deltaTime)
        {
            if (updateVisibleChunkList)
            {
                UpdateVisibleChunkList();
                UpdateLoadChunkQueue();
                updateVisibleChunkList = false;
            }

            if (loadChunkQueue.Count > 0)
            {
                LoadChunksCycle(deltaTime);
            }

            if (world.Config.GenerateChunkMeshes && instantiateChunkQueue.Count > 0)
            {
                InstantiateChunksCycle(deltaTime);
            }

            if (updateChunkQueue.Count > 0)
            {
                UpdateChunksCycle(deltaTime);
            }
        }

        public void ForceUpdate()
        {
            updateVisibleChunkList = true;
        }

        public void Reset()
        {
            visibleChunkList.Clear();
            chunkObjectMap.Clear();
            loadChunkQueue.Clear();
            instantiateChunkQueue.Clear();
            updateChunkQueue.Clear();
        }

        public ChunkState GetChunkState(ChunkIndex index)
        {
            return chunkObjectMap[index].State;
        }

        private void UpdateVisibleChunkList()
        {
            visibleChunkList.Clear();

            int visibleDistance = world.Config.RenderDistance + 3;

            int visibleDistanceSquared = visibleDistance * visibleDistance;
            int loadRangeSquared = (visibleDistance - 1) * (visibleDistance - 1);
            int instantiateRangeSquared = (visibleDistance - 3) * (visibleDistance - 3);

            for (int z = -visibleDistance; z <= visibleDistance; z++)
            {
                for (int x = -visibleDistance; x <= visibleDistance; x++)
                {
                    int distanceSquared = x * x + z * z;
                    if (distanceSquared <= visibleDistanceSquared)
                    {
                        ChunkIndex chunkIndex = originIndex + new ChunkIndex(x, z);
                        visibleChunkList.Add(chunkIndex);

                        ChunkObject chunkObject;
                        if (chunkObjectMap.ContainsKey(chunkIndex))
                        {
                            chunkObject = chunkObjectMap[chunkIndex];
                        }
                        else
                        {
                            chunkObject = new ChunkObject();
                            chunkObjectMap.Add(chunkIndex, chunkObject);
                        }

                        if (distanceSquared <= loadRangeSquared)
                        {
                            chunkObject.State |= ChunkState.READY_FOR_LOAD;
                            if (distanceSquared <= instantiateRangeSquared)
                            {
                                chunkObject.State |= ChunkState.READY_FOR_INSTANTIATE;
                            }
                        }
                    }
                }
            }

            visibleChunkList.Sort((s1, s2) => Vector2.SqrMagnitude(originIndex - s1)
                                             .CompareTo(Vector2.SqrMagnitude(originIndex - s2)));
        }

        private void UpdateLoadChunkQueue()
        {
            foreach (ChunkIndex chunkIndex in visibleChunkList)
            {
                if (chunkObjectMap[chunkIndex].State.HasFlag(ChunkState.READY_FOR_LOAD) &&
                    !map.ContainsChunk(chunkIndex) && !loadChunkQueue.Contains(chunkIndex))
                {
                    loadChunkQueue.Enqueue(chunkIndex);
                }
            }
        }

        private void LoadChunksCycle(float deltaTime)
        {
            loadChunkCycleClock += deltaTime;
            if (loadChunkCycleClock > world.Config.LoadChunkInterval || world.Config.AsyncUpdate)
            {
                loadChunkCycleClock = 0;
                int loadedChunkCount = 0;
                while (loadChunkQueue.Count > 0 && loadedChunkCount < world.Config.LoadChunkBatchSize)
                {
                    ChunkIndex chunkIndexToLoad = loadChunkQueue.Dequeue();
                    LoadChunk(chunkIndexToLoad);
                    loadedChunkCount++;

                    if (chunkObjectMap[chunkIndexToLoad].State.HasFlag(ChunkState.READY_FOR_INSTANTIATE))
                    {
                        instantiateChunkQueue.Enqueue(chunkIndexToLoad);
                    }
                }
            }
        }

        private void InstantiateChunksCycle(float deltaTime)
        {
            instantiateChunkCycleClock += deltaTime;
            if (instantiateChunkCycleClock > world.Config.InstantiateChunkInterval || world.Config.AsyncUpdate)
            {
                instantiateChunkCycleClock = 0;
                int instantiatedChunkCount = 0;
                while (instantiateChunkQueue.Count > 0 && instantiatedChunkCount < world.Config.InstantiateChunkBatchSize)
                {
                    ChunkIndex chunkIndexToInstantiate = instantiateChunkQueue.Dequeue();
                    InstantiateChunk(chunkIndexToInstantiate);
                    instantiatedChunkCount++;
                }
            }
        }

        private void UpdateChunksCycle(float deltaTime)
        {
            updateChunkCycleClock += deltaTime;
            if (updateChunkCycleClock > world.Config.UpdateChunkInterval || world.Config.AsyncUpdate)
            {
                updateChunkCycleClock = 0;
                int updatedChunkCount = 0;
                while (updateChunkQueue.Count > 0 && updatedChunkCount < world.Config.UpdateChunkBatchSize)
                {
                    ChunkIndex chunkIndexToUpdate = updateChunkQueue.Dequeue();
                    UpdateChunk(chunkIndexToUpdate);
                    updatedChunkCount++;
                }
            }
        }

        private void LoadChunk(ChunkIndex chunkIndex)
        {
            Chunk chunk = map.CreateChunk(chunkIndex);
            worldGenerator.GenerateBaseTerrain(chunk.Index);

            ChunkObject chunkObject = chunkObjectMap[chunkIndex];
            chunkObject.Chunk = chunk;
            chunkObject.State |= ChunkState.LOADED;
            chunkObjectMap[chunkIndex] = chunkObject;
        }

        private void InstantiateChunk(ChunkIndex chunkIndex)
        {
            Chunk chunk = map[chunkIndex];

            GameObject chunkGameObject = new GameObject(worldObject.transform.childCount + " - " + chunk.Index,
                typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider)) {tag = "Chunk"};

            chunkGameObject.GetComponent<MeshRenderer>().material = (Material)Resources.Load("Grass", typeof(Material));
            chunkGameObject.transform.position = new Vector3(chunkIndex.X * map.ChunkSize.x, 0, chunkIndex.Y * map.ChunkSize.z);

            ChunkMesh chunkMesh = meshGenerator.GenerateChunkMesh(chunk.Index);
            chunkGameObject.GetComponent<MeshFilter>().mesh = chunkMesh.Mesh;
            //chunkGameObject.GetComponent<MeshCollider>().sharedMesh = chunkMesh.Mesh;
            chunkGameObject.transform.SetParent(worldObject.transform, false);
            //chunkGameObject.hideFlags = HideFlags.HideInHierarchy;

            ChunkObject chunkObject = chunkObjectMap[chunkIndex];
            chunkObject.Object = chunkGameObject;
            chunkObject.State |= ChunkState.INSTANTIATED;
            chunkObjectMap[chunkIndex] = chunkObject;
        }

        private void UpdateChunk(ChunkIndex chunkIndex)
        {
            Chunk chunk = map[chunkIndex];
            ChunkMesh chunkMesh = meshGenerator.GenerateChunkMesh(chunkIndex);

            GameObject chunkObject = chunkObjectMap[chunkIndex].Object;
            chunkObject.GetComponent<MeshFilter>().mesh = chunkMesh.Mesh;
            chunkObject.GetComponent<MeshCollider>().sharedMesh = chunkMesh.Mesh;

            chunkObjectMap[chunkIndex].Dirty = false;
        }

        public void SetDirty(ChunkIndex chunkIndex)
        {
            if (!chunkObjectMap[chunkIndex].Dirty)
            {
                chunkObjectMap[chunkIndex].Dirty = true;
                updateChunkQueue.Enqueue(chunkIndex);
            }
        }

        public void OnDebug()
        {
            Vector3 size = map.ChunkSize;
            foreach (ChunkIndex chunkIndex in visibleChunkList)
            {
                ChunkObject chunkObject = chunkObjectMap[chunkIndex];
                Color cubeColor = chunkObject.State.HasFlag(ChunkState.LOADED) ? (chunkObject.State.HasFlag(ChunkState.INSTANTIATED) ? Color.green : Color.yellow) : Color.gray;
                Vector3 position = new Vector3(chunkIndex.X * map.ChunkSize.x, 0, chunkIndex.Y * map.ChunkSize.z);
                Debug.DrawWireCube(position + size / 2, size * .99f, cubeColor);
            }
        }
    }
}