﻿using System.Net;
using Overnight.WorldEngine;
using Overnight.WorldEngine.Core;
using UnityEngine;

public class WorldEditor : MonoBehaviour
{
	#region STATICS
	#endregion

	#region COMPONENTS
    public WorldEngine WorldEngine;
    #endregion

    #region GAMEOBJECTS
    #endregion

    #region VARIABLES
    private World world;
    private Map map;
    private ChunkManager chunkManager;
    #endregion

    #region PROPERTIES
    #endregion

    void Start()
    {
        world = WorldEngine.GetWorld();
        map = world.GetMap();
        chunkManager = world.GetChunkManager();
    }

    public void EditBlockDensity(BlockIndex blockIndex, float density)
    {
        ChunkIndex chunkIndex = blockIndex.GetChunkIndex();
        map[chunkIndex].SetBlockDensity(blockIndex, density);
        if (chunkManager.GetChunkState(chunkIndex).HasFlag(ChunkState.INSTANTIATED))
        {
            chunkManager.SetDirty(chunkIndex);
        }
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            AddBlockDensityAtMouse(0.01f, 2);
        }

        if (Input.GetMouseButton(1))
        {
            AddBlockDensityAtMouse(-0.01f, 2);
        }
    }

    private void AddBlockDensityAtMouse(float density, int range)
    {
        Vector3 mousePosition = GetMousePosition(density > 0);
        (Block block, BlockIndex blockIndex) = map.GetNearestBlock(mousePosition);

        foreach ((Block currentBlock, BlockIndex currentIndex) in map.GetIterator(blockIndex, Vector3Int.one * range))
        {
            if (currentIndex.Y >= 0 && currentIndex.Y < map.ChunkSize.y)
            {
                 ChunkIndex chunkIndex = currentIndex.GetChunkIndex();
                 map[chunkIndex].SetBlockDensity(currentIndex, currentBlock.Density + density);
                 if (chunkManager.GetChunkState(chunkIndex).HasFlag(ChunkState.INSTANTIATED))
                 {
                     chunkManager.SetDirty(chunkIndex);
                 }
            }
        }
    }

    private Vector3 GetMousePosition(bool add)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            return hit.point; //+ (add ? hit.normal : -hit.normal) / 2;
        }
        
        return Vector3.zero;
    }
}

