﻿namespace Overnight.WorldEngine.Core
{
    /// <summary>
    ///   <para>A Block defines the structural properties of a fixed size volume inside the space.</para>
    /// </summary>
    public struct Block
    {
        #region
        /// <summary>
        ///   <para>Defines empty volume with initial parameters.</para>
        /// </summary>
        public static Block Empty { get; } = new Block(0, 0);
        #endregion

        #region VARIABLES
        /// <summary>
        ///   <para>Property of volume that differentiates the spatial characteristics of this block from others.</para>
        /// </summary>
        public byte ID;

        /// <summary>
        ///   <para>Spatial density of this volume.</para>
        /// </summary>
        public float Density;
        #endregion

        public Block(byte id, float density)
        {
            ID = id;
            Density = density;
        }

        /// <summary>
        ///   <para>Returns a nicely formatted string for this Block.</para>
        /// </summary>
        public override string ToString()
        {
            return $"(ID : {ID} - Density : {Density})";
        }
    }
}
