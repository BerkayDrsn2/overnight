﻿using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class World
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private Map map;
        private ChunkManager chunkManager;
        private WorldGenerator worldGenerator;
        #endregion

        #region GAMEOBJECTS
        private GameObject worldObject;
        #endregion

        #region VARIABLES
        public WorldEngineConfig Config;
        #endregion

        #region PROPERTIES
        public Vector3Int ChunkSize { get; }
        #endregion

        public World(WorldEngine worldEngine)
        {
            Config = worldEngine.Config;
            worldObject = worldEngine.gameObject;
            ChunkSize = worldEngine.ChunkSize;
        }

        public void Initialize()
        {
            DestroyChunkMeshes();
            map = new Map(ChunkSize);
            worldGenerator = new WorldGenerator(this);
            chunkManager = new ChunkManager(this);
        }

        public void Update()
        {
            chunkManager.Update(Time.deltaTime);
        }

        public void ForceUpdate()
        {
            chunkManager.ForceUpdate();
        }

        public Map GetMap()
        {
            return map;
        }

        public WorldGenerator GetWorldGenerator()
        {
            return worldGenerator;
        }

        public ChunkManager GetChunkManager()
        {
            return chunkManager;
        }

        public GameObject GetWorldObject()
        {
            return worldObject;
        }

        public void Reset()
        {
            DestroyChunkMeshes();
            map.Clear();
            chunkManager.Reset();
        }

        public void OnDebug()
        {
            chunkManager.OnDebug();
        }

        private void DestroyChunkMeshes()
        {
            for (int i = worldObject.transform.childCount - 1; i >= 0; i--)
            {
                Object.DestroyImmediate(worldObject.transform.GetChild(i).gameObject);
            }
        }
    }
}
