﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class BlockIterator : IEnumerable<(Block, BlockIndex)>
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private readonly Map map;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        private Vector3Int chunkSize;

        private readonly IEnumerator<(Block, BlockIndex)> iterator;
        #endregion

        #region PROPERTIES
        #endregion

        public BlockIterator(Map map, ChunkIndex chunkIndex)
        {
            this.map = map;
            chunkSize = map.ChunkSize;
            iterator = Iterator(chunkIndex);
        }

        public BlockIterator(Map map, BlockIndex blockIndex, Vector3Int size, bool biDirectional)
        {
            this.map = map;
            chunkSize = map.ChunkSize;
            iterator = Iterator(blockIndex, size, biDirectional);
        }

        public BlockIterator(Map map, BlockIndex blockIndex, IList<Vector3Int> indexList)
        {
            this.map = map;
            chunkSize = map.ChunkSize;
            iterator = Iterator(blockIndex, indexList);
        }

        public IEnumerator<(Block, BlockIndex)> Iterator(ChunkIndex chunkIndex)
        {
            Chunk chunk = map[chunkIndex];

            for (int y = 0; y < chunkSize.y; y++)
            {
                for (int z = 0; z < chunkSize.z; z++)
                {
                    for (int x = 0; x < chunkSize.x; x++)
                    {
                        yield return (chunk[x, y, z], new BlockIndex(x, y, z, chunkIndex));
                    }
                }
            }
        }

        public IEnumerator<(Block, BlockIndex)> Iterator(BlockIndex blockIndex, Vector3Int size, bool biDirectional)
        {
            Chunk currentChunk = map[blockIndex.GetChunkIndex()];

            for (int y = biDirectional ? -size.y : 0; y <= size.y; y++)
            {
                for (int z = biDirectional ? -size.z : 0; z <= size.z; z++)
                {
                    for (int x = biDirectional ? -size.x : 0; x <= size.x; x++)
                    {
                        BlockIndex adjustedIndex = map.AdjustIndex(blockIndex + new Vector3Int(x, y, z));

                        if (adjustedIndex.Y < 0)
                        {
                            yield return (Block.Empty, adjustedIndex);
                        }
                        else if (adjustedIndex.Y > map.ChunkSize.y - 1)
                        {
                            yield return (Block.Empty, adjustedIndex);
                        }
                        else
                        {
                            if (currentChunk.Index != adjustedIndex.GetChunkIndex())
                            {
                                currentChunk = map[adjustedIndex.GetChunkIndex()];
                            }
                            
                            yield return (currentChunk[adjustedIndex], adjustedIndex);
                        }
                    }
                }
            }
        }

        public IEnumerator<(Block, BlockIndex)> Iterator(BlockIndex blockIndex, IList<Vector3Int> indexList)
        {
            Chunk currentChunk = map[blockIndex.GetChunkIndex()];
            foreach (Vector3Int indexOffset in indexList)
            {
                BlockIndex adjustedIndex = map.AdjustIndex(blockIndex + indexOffset);

                if (adjustedIndex.Y < 0)
                {
                    yield return (Block.Empty, adjustedIndex);
                }
                else if (adjustedIndex.Y > map.ChunkSize.y - 1)
                {
                    yield return (Block.Empty, adjustedIndex);
                }
                else
                {
                    if (currentChunk.Index != adjustedIndex.GetChunkIndex())
                    {
                        currentChunk = map[adjustedIndex.GetChunkIndex()];
                    }

                    yield return (currentChunk[adjustedIndex], adjustedIndex);
                }
            }
        }

        public IEnumerator<(Block, BlockIndex)> GetEnumerator()
        {
            return iterator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

