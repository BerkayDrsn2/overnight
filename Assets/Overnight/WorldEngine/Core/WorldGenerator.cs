﻿using UnityEngine;

namespace Overnight.WorldEngine.Core
{
    public class WorldGenerator
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private World world;
        private NoiseGenerator noiseGenerator;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        private readonly Map map;
        #endregion

        #region PROPERTIES
        #endregion

        public WorldGenerator(World world)
        {
            this.world = world;
            map = world.GetMap();
            noiseGenerator = new NoiseGenerator(world, true);
        }

        public void GenerateBaseTerrain(ChunkIndex chunkIndex)
        {
            Chunk chunk = map[chunkIndex];

            //float[,,] noise = noiseGenerator.GenerateNoise(chunkIndex, 0, 0);
            foreach ((Block _, BlockIndex index) in map.GetIterator(chunkIndex))
            {
                float density = Noise.GetOctaveNoise((chunkIndex.X * map.ChunkSize.x + index.X) / world.Config.Scale.x,
                                                     index.Y / world.Config.Scale.y,
                                                     (chunkIndex.Y * map.ChunkSize.z + index.Z) / world.Config.Scale.z,
                                                        world.Config.Octave);
                float seaLevel = 1 - (float)index.Y / map.ChunkSize.y;

                density = Mathf.Clamp01((seaLevel + density) / 2);
                
                //density = noise[index.X, index.Y, index.Z];

                chunk.SetBlockDensity(index, density);
            }
            Debug.Log("Done");
        }
    }
}
