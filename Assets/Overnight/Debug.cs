﻿using System.Diagnostics;
using UnityEngine;

namespace Overnight
{
    public static class Debug
    {
        #region COMPONENTS
        private static GLDrawer glDrawer;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region VARIABLES
        private static bool logEnabled = true;
        private static bool glEnabled = true;
        #endregion

        #region PROPERTIES
        public static bool LogEnabled
        {
            get => logEnabled;
            set => logEnabled = value;
        }

        public static bool GLEnabled
        {
            get => glEnabled;
            set
            {
                if (value && glDrawer == null)
                {
                    InitializeGLDrawer();
                }
                glEnabled = value;
            }
        }
        #endregion

        public static void InitializeGLDrawer()
        {
            Camera camera = Object.FindObjectOfType<Camera>();
            glDrawer = camera.gameObject.AddComponent<GLDrawer>();
        }

        [Conditional("LOG_ENABLED")]
        public static void Log(object message, bool logEnabled = true)
        {
            if (Debug.logEnabled && logEnabled)
            {
                UnityEngine.Debug.Log(message);
            }
        }

        [Conditional("ASSERT_ENABLED")]
        public static void Assert(bool condition)
        {
            if (condition)
                return;

            UnityEngine.Debug.Assert(false);

            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
            else
            {
                UnityEngine.Debug.Break();
            }
        }

        [Conditional("ASSERT_ENABLED")]
        public static void Assert(bool condition, object message)
        {
            if (condition)
                return;

            UnityEngine.Debug.Assert(false, message);

            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
            else
            {
                UnityEngine.Debug.Break();
            }
        }

        [Conditional("GL_ENABLED")]
        public static void DrawLine(Vector3 start, Vector3 end, bool glEnabled = true)
        {
            if (Debug.glEnabled && glEnabled)
            {
                glDrawer.DrawLine(start, end);
            }
        }

        [Conditional("GL_ENABLED")]
        public static void DrawLine(Vector3 start, Vector3 end, Color color, bool glEnabled = true)
        {
            if (Debug.glEnabled && glEnabled)
            {
                glDrawer.DrawLine(start, end, color);
            }
        }

        [Conditional("GL_ENABLED")]
        public static void DrawLine(Vector3 start, Vector3 end, float thickness, bool glEnabled = true)
        {
            if (Debug.glEnabled && glEnabled)
            {
                glDrawer.DrawLine(start, end, Color.black, thickness);
            }
        }

        [Conditional("GL_ENABLED")]
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float thickness, bool glEnabled = true)
        {
            if (Debug.glEnabled && glEnabled)
            {
                glDrawer.DrawLine(start, end, color, thickness);
            }
        }

        [Conditional("GL_ENABLED")]
        public static void DrawWireCube(Vector3 origin, Vector3 size, Color color, bool glEnabled = true)
        {
            if (Debug.glEnabled && glEnabled)
            {
                Vector3 v1 = origin - size / 2;
                Vector3 v2 = v1 + size.x * Vector3.right;
                Vector3 v3 = v1 + size.z * Vector3.forward;
                Vector3 v4 = v1 + size.x * Vector3.right + size.z * Vector3.forward;
                Vector3 v5 = origin + size / 2;
                Vector3 v6 = v5 - size.x * Vector3.right;
                Vector3 v7 = v5 - size.z * Vector3.forward;
                Vector3 v8 = v5 - size.x * Vector3.right - size.z * Vector3.forward;

                glDrawer.DrawLine(v1, v2, color);
                glDrawer.DrawLine(v1, v3, color);
                glDrawer.DrawLine(v2, v4, color);
                glDrawer.DrawLine(v3, v4, color);

                glDrawer.DrawLine(v5, v6, color);
                glDrawer.DrawLine(v5, v7, color);
                glDrawer.DrawLine(v6, v8, color);
                glDrawer.DrawLine(v7, v8, color);

                glDrawer.DrawLine(v1, v8, color);
                glDrawer.DrawLine(v2, v7, color);
                glDrawer.DrawLine(v3, v6, color);
                glDrawer.DrawLine(v4, v5, color);
            }
        }
    }
}

